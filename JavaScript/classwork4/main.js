/* ЗАДАНИЕ - 1
 * Написать функцию суммирования.
 * Принимает аргументы: первое число и второе число
 * Возвращаемое функцией значение: сумма двух аргументов*/

// function calc(first, second) {
// 	return first + second;
// }

/* ЗАДАНИЕ - 2
 * Написать функцию которая будет принимать два аргумента - число с которого НАЧАТь отсчет и число до которого нужно досчитать.
 * Под отсчетом имеется в виду последовательный вывод чисел в консоль с увеличением на единицу.
 */

// function eee(first, second) {
// 	for (let i = first; i <= second; i++) {
// 		console.log(i);
// 	}
// }

// eee(1, 10);

/* ЗАДАНИЕ - 3
 * Написать функцию, которая будет суммировать ВСЕ числа, которые будут переданы ей в качестве аргументов.
 * */

/* ЗАДАНИЕ - 4
 * Написать функцию, которая проверяет количество переданных агрументов.
 * Принимаемые аргументы - три числа
 * Если число переданых аргументов не равно трем - выводить сообщение с текстом "Функция вызвана с *КОЛИЧЕСТВО ПЕРЕДАННЫХ АРГУМЕНТОВ* параметрами!"
 * */

/* ЗАДАНИЕ - 5
 * Написать функцию calculateResidue, которая будет выводить в консоль ВСЕ числа в заданом диапазоне, которые кратны третьему числу.
 * Аргументы функции:
 *   1) число С которого начинается диапазон (включительно)
 *   2) число которым диапазон заканчивается (включительно)
 *   3) число КРАТНЫЕ КОТОРОМУ нужно вывести в консоль
 * Возвращаемое значение - отсутствует
 * В идеале разделить это на 3 фнукциию
 * 1 - спрашивает два числа(диапазон),
 * 2 - спрашивает третье число и выводит в консоль результаты,
 * 3 - сборная солянка из предыдущих двух.
 * */

let a = +prompt('From: ');
let b = +prompt('To: ');
let c = +prompt('Na chto delit? ');

function superNigger(dpFirst, dpSecond, divis) {
	for (let i = dpFirst; i <= dpSecond; i++) {
		if (i % divis === 0) {
			console.log(i);
		}
	}
}

superNigger(a, b, c);
/* ЗАДАНИЕ - 6
 * Написать функцию getPrice(), которая будет вычислять стоимость бургера в зависимости от того какой у него размер и начинка.
 * Размер и начинку определяет пользователь. УСЛОВНОЕ УПРОЩЕНИЕ - он умеет вводить ТОЛЬКО имена констант, т.е. проверка на некорректность введения данных не требуется.
 *
 * Размеры и начинки записаны в константы:*/
const SIZE_SMALL = {
	name: 'small',
	price: 15,
	cal: 250,
};

const SIZE_LARGE = {
	name: 'large',
	price: 25,
	cal: 340,
};

const STUFFING_CHEASE = {
	name: 'chease',
	price: 4,
	cal: 25,
};

const STUFFING_SALAD = {
	name: 'salad',
	price: 5,
	cal: 5,
};

const STUFFING_BEEF = {
	name: 'beef',
	price: 10,
	cal: 50,
};

function getPrice(size, stuffing) {
	let price = 0;
	let callories = 0;
	switch (size) {
		case SIZE_SMALL.name:
			price += SIZE_SMALL.price;
			callories += SIZE_SMALL.cal;
			break;
		case SIZE_LARGE.name:
			price += SIZE_LARGE.price;
			callories += SIZE_LARGE.cal;
			break;
	}

	switch (stuffing) {
		case STUFFING_CHEASE.name:
			price += STUFFING_CHEASE.price;
			callories += STUFFING_CHEASE.cal;
			break;
		case STUFFING_BEEF.name:
			price += STUFFING_BEEF.price;
			callories += STUFFING_BEEF.cal;
			break;
		case STUFFING_SALAD.name:
			price += STUFFING_SALAD.price;
			callories += STUFFING_SALAD.cal;
			break;
	}

	return {
		price: price,
		cal: callories,
	};
	// return {
	//   price,
	//   callories
	// };
}

// console.log(getPrice('large', 'chease'));

// console.log(getPrice(
//     prompt('Enter the size of burger'),
//     prompt('Enter stuffing name')
// ));

/* ПРОДВИНУТАЯ СЛОЖНОСТЬ - переписать этот код с использованием функцию-конструктора Burger.
 * Эта функция должна создавать екземпляр обекта, в котором сразу есть все константы
 * И метод getPriceAndCalories, в котором помимо стоимости высчитывается еще и каллорийность
 * */

// function Burger (size, stuffing) {
//   this.size = size;
//   this.stuffing = stuffing;
// }
//
// console.log(new Burger(SIZE_LARGE.name, STUFFING_BEEF.name));
//
// const myBurger = new Burger('extra large', 'no chease');
// console.log(myBurger);
