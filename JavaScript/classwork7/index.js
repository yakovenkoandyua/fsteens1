// const element = document.getElementById('text1');
// const btn = document.getElementById('submit');
// console.dir(element);
// btn.addEventListener('click', e => {
// 	e.preventDefault();
// 	if (element.value.length < 3) {
// 		// element.className = 'error';
// 		element.classList.add('error');
// 	} else {
// 		element.classList.add('confirm');
// 	}
// });

// const listArray = ['машина', 'daint'];
// function showArray(array) {
// 	const div = document.querySelector('.content');
// 	const list = document.createElement('ul');
// 	array.forEach(function (item) {
// 		const elementLi = document.createElement('li');
// 		elementLi.textContent = item;
// 		list.prepend(elementLi);
// 	});

// 	div.append(list);
// }
// showArray(listArray);

let word = 'костя';

function createSquare(param) {
	const container = document.querySelector('.hangman');
	for (let i = 0; i < param.length; i++) {
		const text = document.createElement('p');
		text.className = 'text';
		container.append(text);
	}
}
createSquare(word);
let counter = 10;
let array = word.split('');
let arr = array;
const attCounter = document.querySelector('.attempt');
attCounter.textContent = `Attempts remaining ${counter}!`;
function hangMan(param, letter) {
	console.log(array);
	const allLetter = document.querySelectorAll('.text');
	let x = arr.length;
	array.forEach(function (item, index) {
		if (item === letter) {
			arr.splice(index, 1);
			allLetter[index].textContent = item;
		}
	});
	let y = arr.length;
	if (x === y) {
		counter--;
		attCounter.textContent = `Attempts remaining: ${counter}!`;
	}
}
const element = document.getElementById('word');
const btn = document.getElementById('submit');
btn.addEventListener('click', e => {
	e.preventDefault();
	hangMan(word, element.value);
});
