/* ЗАДАНИЕ - 1
 * Написать функцию, которая принимает 2 аргумента - имя и возраст пользователя
 * Возвращаемое значение этой функции - объект с двумя ключами name и age, куда будут записаны значения переданных функции аргументов.
 * */

// const name = prompt('введи свое имя');
// const age = +prompt('введи свой возвраст');

// function createUser(userName, userAge) {
// 	const user = {
// 		name: userName,
// 		age: userAge,
// 	};

// 	return user;
// }

// console.log(createUser(name, age));

/* ЗАДАНИЕ - 2
 * Добавить к предыдущему заданию функционал.
 * В возвращаемом объекте должен быть метод, который увеличивает возраст на 1.
 * Т.е. внутри объекта будет свойство\ключ, значением которого будет являться функция,
 * которая увеличивает поле age ЭТОГО объекта на 1
 * */

const name = prompt('введи свое имя');
const age = +prompt('введи свой возвраст');

function createUser(userName, userAge) {
	const user = {
		name: userName,
		age: userAge,
		plusAge: function () {
			this.age += 1;
		},
	};

	return user;
}

let user = createUser(name, age);
console.log(user);
user.plusAge();
console.log(user);
/* ЗАДАНИЕ - 3
 * Добавить к предыдущему заданию функционал.
 * В возвращаемом объекте должен появиться еще один метод - addField(). Он будет добавлять свойства в объект.
 * Т.е. внутри объекта будет еще одно свойство\ключ, значением которого будет являться функция.
 * Эта функция принимает два аргумента:
 *   1 - имя свойства, которое будет создаваться
 *   2 - значение. которое туда должно быть записано
 * */
// const name = prompt('введи свое имя');
// const age = +prompt('введи свой возвраст');

// function createUser(userName, userAge) {
// 	const user = {
// 		name: userName,
// 		age: userAge,
// 		plusAge: function () {
// 			user.age += 1;
// 		},
// 		addField: function (key, value) {
// 			// console.log(' параметр key при вызове функции addField', key);
// 			// console.log(' параметр value при вызове функции addField', value);
// 			user[key] = value;
// 		},
// 	};

// 	return user;
// }

// let user = createUser(name, age);
// user.addField('job', 'profesor');
// user.addField('car', 'ferarri');

// console.log(user);

/* ЗАДАНИЕ - 4
 * Переписать предыдущее задание на функцию-конструктор.
 * Т.е. раньше внутри функции создавался объект, в который добавлялись свойства.
 * Теперь нужно, чтобы функция описывала алгоритм создания нового объекта и создавала объект типа User
 * и происходить это будет при использовании оператора new.
 * */

/* ЗАДАНИЕ - 5
 * Снова Бургер.
 * Есть объект с базовыми значениями размера и начинки бургера. Этот объект мы считаем статическим
 * не изменяем ничего из того что в нем содержится, только пользуемся тем что внутри
 * */
const constantValues = {
	SIZE_SMALL: {
		price: 15,
		cal: 250,
	},

	SIZE_LARGE: {
		price: 25,
		cal: 340,
	},

	STUFFING_CHEASE: {
		price: 4,
		cal: 25,
	},

	STUFFING_SALAD: {
		price: 5,
		cal: 5,
	},

	STUFFING_BEEF: {
		price: 10,
		cal: 50,
	},
};
/* НУЖНО
 * Написать функцию объекта типа Burger, которая принимает два аргумента - размер и начинку бургера.
 * Для удобства считаем что оба эти аргумента 100% будут переданы строками в которых написано одно из названий свойств объекта constantValues.
 * У итогового объекта должны быть:
 *   1 - поля size и stuffing
 *   2 - метод getPrice(), который создает в объекте бургера свойство price и вычисляет стоимость бургера, основываясь на введенных в функцию  аргументах и значениях в объекте constantValues
 *   3 - метод getCalories(), который создает в объекте бургера свойство calories и вычисляет калорийность бургера, основываясь на введенных в  конструктор аргументах и значениях в объекте constantValues
 * */
//

let burgerSize = prompt('Size your burger: ');
let burgerTopping = prompt('Add topping to your burger: ');

function getBurger(burgerSize, burgerTopping) {
	const burgerOrder = {
		size: burgerSize,
		topping: burgerTopping,
		getPrice: function () {
			burgerOrder.finalPrice =
				constantValues[burgerSize].price + constantValues[burgerTopping].price;
		},
		getCalories: function () {
			burgerOrder.finalCalories =
				constantValues[burgerSize].cal + constantValues[burgerTopping].cal;
		},
	};
	return burgerOrder;
}

let burgerMake = getBurger(burgerSize, burgerTopping);

burgerMake.getPrice();
burgerMake.getCalories();
console.log(burgerMake);
