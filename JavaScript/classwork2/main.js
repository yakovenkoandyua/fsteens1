/* ЗАДАНИЕ - 1
 * Пользователь вводит в модальное окно любое число. Нужно вывести на экран "Четное" или "Не четное" в зависимости от того четное число или нет.
 */
// const userNumber = +prompt('Enter Number');
// if (userNumber % 2 === 0) {
// 	console.log('четное');
// } else {
// 	console.log('не четнок');
// }

/* ЗАДАНИЕ - 2
 * Спросить пользователя на каком языке он хочет увидеть список дней недели. Пользователь может ввести  - ru, en, ukr
 * Вывести на экран список дней недели на выбранном языке.
 * */
// const ukr = 'ukr',
// 	en = 'en';
// ru = 'ru';
// const lang = prompt('какой язык');
// if (lang === ukr) {
// 	alert('понэдилок');
// } else if (lang === en) {
// 	alert('monday');
// } else {
//     alert('поендельник');
// }

// switch (lang) {
// 	case ukr:
// 		alert('понэдилок');
// 		break;
// 	case en:
// 		alert('monday');
// 		break;
// 	case ru:
// 		alert('поендельник');
// }

/* ЗАДАНИЕ - 3
 * Пользователь вводит 3 числа. Нужно вывести на экран собщение с максимальным числом из введенных
 * */
// let first = +prompt('first');
// let second = +prompt('second');
// let third = +prompt('third');

// const maxNumber = Math.max(first, second, third);
// alert(maxNumber);
// if (first > second) {
// 	debugger;
// 	if (first > third) {
// 		alert(`${first}`);
// 	} else {
// 		alert(`${third}`);
// 	}
// } else if (third > second) {
// 	alert(`${third}`);
// } else {
// 	alert(`${second}`);
// }
/* ЗАДАНИЕ - 4
 * Имитируем список персонала, где у каждого человека есть своя роль. Пользователь вводит имя, после чего нужно вывести сообщение с ролью пользователя.
 * Список следующий:
 * Boss - главный Boss
 * Boss Juniior - правая рука Boss'a
 * Gogol Mogol - далает Гоголь-Моголь всем сотрудникам
 * John - уборщик
 * */

// const boss = 'главный босс';
// const bossJunior = 'правая рука Boss';
// const gogol = 'далает Гоголь-Моголь всем сотрудникам';
// const john = 'уборщик';

// const user = prompt('введи проффесию');

// if (user === 'Boss') {
// 	console.log(boss);
// } else if (user === 'Boss Juniior') {
// 	console.log(bossJunior);
// } else if (user === 'Gogol Mogol') {
// 	console.log(gogol);
// } else if (user === 'John') {
// 	console.log(john);
// }

// switch (user) {
// 	case 'Boss':
// 		console.log(boss);
// 		break;
// 	case 'Boss Juniior':
// 		console.log(bossJunior);
// 		break;
// 	case 'Gogol Mogol':
// 		console.log(gogol);
// 		break;
// 	case 'John':
// 		console.log(john);
// }

/* ЗАДАНИЕ - 5
 * Напишите программу кофейная машина.
 * Программа принимает монеты и готовит напиток (Кофе 25грн, капучино 50грн, чай 10грн).
 * Т.е. пользователь вводит в модальное окно сумму денег и название напитка.
 * В зависимости от того какое название было введено - нужно вычислить сдачу и вывести сообщение: "Ваш напиток *НАЗВАНИЕ НАПИТКА* и сдача *СУММА СДАЧИ*"
 * В случае если пользователь ввел сумму без сдачи - выводить сообщение: "Ваш напиток *НАЗВАНИЕ НАПИТКА*. Спасибо за точную сумму!))"
 * */

const priceList = {
	coffePrice: 25,
	cappuPrice: 50,
	teaPrice: 10,
	sugarPrice: 2,
	syropPrice: 4,
	creamPrice: 7,
};

const userExperience = {
	sumPrice: 0,
	drinkName: prompt('Chto vam nado?'),
	checkDrink: function () {
		while (
			this.drinkName !== 'Чай' &&
			this.drinkName !== 'Кофе' &&
			this.drinkName !== 'Каппучино'
		) {
			this.drinkName = prompt('Vvedi sushestvuiushiy napitok: ');
		}
	},
	drinkAddPrice: function () {
		switch (this.drinkName) {
			case 'Кофе':
				this.sumPrice += priceList.coffePrice;
				break;
			case 'Каппучино':
				this.sumPrice += priceList.cappuPrice;
				break;
			case 'Чай':
				this.sumPrice += priceList.teaPrice;
				break;
		}
	},
	toppingAddPrice: function () {
		if (confirm('Do you want nemnogo dobavok? ')) {
			let addon = prompt('Kakuiu?');
			switch (addon) {
				case 'Сахар':
					this.sumPrice += priceList.sugarPrice;
					break;
				case 'Сироп':
					this.sumPrice += priceList.syropPrice;
					break;
				case 'Сливки':
					this.sumPrice += priceList.creamPrice;
			}
			while (confirm('Xotite eshe?')) {
				let addon = prompt('Kakuiu?');
				switch (addon) {
					case 'Сахар':
						this.sumPrice += priceList.sugarPrice;
						break;
					case 'Сироп':
						this.sumPrice += priceList.syropPrice;
						break;
					case 'Сливки':
						this.sumPrice += priceList.creamPrice;
				}
			}
		}
	},
	sumPriceCount: function () {
		let moneyGiven = +prompt('Skolko dash? ');
		if (moneyGiven < this.sumPrice) {
			while (moneyGiven < this.sumPrice) {
				moneyGiven += +prompt(`Nehvataet ${this.sumPrice - moneyGiven} rublev dai eshe`);
			}
		}
		if (moneyGiven === this.sumPrice) {
			alert('Spasibo baz zdachi');
		} else {
			alert(`Vasha zdacha ${moneyGiven - this.sumPrice}`);
		}
	},
};

userExperience.checkDrink();
userExperience.drinkAddPrice();
userExperience.toppingAddPrice();
userExperience.sumPriceCount();

console.log(userExperience);
