// Написать функцию filterBy(), которая будет принимать в себя 2 аргумента.
// Первый аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип данных.
// Функция должна вернуть новый массив, который будет содержать в себе все данные,
// которые были переданы в аргумент, за исключением тех, тип которых был передан вторым аргументом.
// То есть, если передать массив ['hello', 'world', 23, '23', null], и вторым аргументом передать 'string',
// то функция вернет массив [23, null].

// let a = 'helo';
// function filterBy(arr, type) {
// 	const newArr = [];
// 	arr.forEach(item => {
// 		if (typeof item !== type) {
// 			newArr.push(item);
// 		}
// 	});
// 	return newArr;
// }

// const filter = (arr, type) => arr.filter(item => typeof item !== type);
// console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));

// const calc = (item, num) => 1 + 3;
// calc();

// let asnwer = prompt('user');
// a = a + ' ' + asnwer;
// alert(a);

// console.log(a);
// const arr = ['hello', 'world', 23, '23', null];
// filterBy(arr, 'string')

// let x = 1;
// console.log('перед тем как поменять', x);
// x = 289;
// console.log('поемнял', x);

// console.log(x + 5);
// const p = document.createElement('p');
// console.log(p);

// p = 'hello';
// console.log(p);
const btn = document.getElementById('btn');
const block = document.querySelector('.block');

btn.addEventListener('click', function () {
	block.style.display = 'block';
});
