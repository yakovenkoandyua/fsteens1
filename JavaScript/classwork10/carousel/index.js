const allImg = document.querySelectorAll('.img');
console.log(allImg);
const arrowLeft = document.querySelector('.left');
const arrowRight = document.querySelector('.right');
const colors = ['#df1f26', '#b427a16e', '#e4e4e4'];
const colorsItems = document.querySelectorAll('.colors-item');
const tabs = document.querySelector('.colors');

let counter = 0;

arrowRight.addEventListener('click', () => {
	allImg[counter].classList.remove('active');
	colorsItems[counter].classList.remove('active');
	counter++;
	if (counter === allImg.length) {
		counter = 0;
	}
	allImg[counter].classList.add('active');
	colorsItems[counter].classList.add('active');
	tabs.style.boxShadow = `0px 4px 18px 0px ${colors[counter]}`;
});
arrowLeft.addEventListener('click', () => {
	allImg[counter].classList.remove('active');
	colorsItems[counter].classList.remove('active');
	if (counter === 0) {
		counter = allImg.length;
	}
	counter--;
	allImg[counter].classList.add('active');
	colorsItems[counter].classList.add('active');
	tabs.style.boxShadow = `0px 4px 18px 0px ${colors[counter]}`;
});

tabs.addEventListener('click', e => {
	if (e.target.classList.contains('colors')) return;
	colorsItems.forEach(item => {
		item.classList.remove('active');
	});
	e.target.classList.add('active');
	allImg.forEach((item, index) => {
		if (e.target.dataset.colors === item.dataset.content) {
			item.classList.add('active');
			counter = index;
			tabs.style.boxShadow = `0px 4px 18px 0px ${colors[counter]}`;
		} else {
			item.classList.remove('active');
		}
	});
});

// BURGER
const hamburger = document.querySelector('.hamburger');
const navLinks = document.querySelector('.nav-links');
const links = document.querySelectorAll('.nav-links li');
const lines = document.querySelectorAll('.line');

hamburger.addEventListener('click', () => {
	navLinks.classList.toggle('open');
	links.forEach(item => {
		item.classList.toggle('fade');
	});
	lines[0].classList.toggle('top');
	lines[1].classList.toggle('medium');
	lines[2].classList.toggle('bot');
});

// CURSOR
let mouseCursor = document.querySelector('.cursor');

window.addEventListener('mousemove', e => {
	console.log('object');
	mouseCursor.style.top = e.pageY + 'px';
	mouseCursor.style.left = e.pageX + 'px';
});
