const btn = document.querySelector('.open');
const modal = document.querySelector('.modal-overlay');
const modalBody = document.querySelector('.modal-body');
const exit = document.querySelector('.head span');
// modalBody.addEventListener('click', e => {
// 	e.stopPropagation();
// });
btn.addEventListener('click', () => {
	modal.classList.add('active');
});

modal.addEventListener('click', e => {
	if (!e.target.classList.contains('modal-overlay')) return;

	modal.classList.remove('active');
});
exit.addEventListener('click', () => {
	modal.classList.remove('active');
});

// ON TOP

const onTop = document.querySelector('.top');

window.addEventListener('scroll', e => {
	let clientHeight = window.innerHeight;
	let windowHeight = window.scrollY;
	if (windowHeight > clientHeight) {
		onTop.style.opacity = '1';
	} else {
		onTop.style.opacity = '0';
	}
});

onTop.addEventListener('click', () => {
	window.scrollTo({
		top: 0,
	});
});
