const swiper = new Swiper('.swiper-container', {
	// Optional parameters
	direction: 'horizontal',
	loop: true,

	// If we need pagination
	pagination: {
		el: '.swiper-pagination',
	},

	// Navigation arrows
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
	},

	// And if we need scrollbar
	scrollbar: {
		el: '.swiper-scrollbar',
	},
});

// import { a } from './main.js';
// import hello from './main.js';

// console.log(a);
// hello();

// let num = 0;
// const title = document.querySelector('.title');

// setInterval(function () {
// 	title.textContent = `Отсчет секунд - ${num}`;
// 	num++;
// 	if (num > 60) {
// 		num = 0;
// 	}
// }, 1000);
// console.log('отсчет начался');
