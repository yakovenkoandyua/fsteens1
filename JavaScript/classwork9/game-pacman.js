const pacman = document.querySelector('.pacman');
let x = 0;
let y = 0;

window.addEventListener('keypress', event => {
	switch (event.key) {
		case 'd':
			x += 10;
			pacman.style.top = y;
			pacman.style.left = x + 'px';
			pacman.style.transform = 'rotate(0deg)';
			break;
		case 'w':
			y -= 10;
			pacman.style.top = y + 'px';
			pacman.style.left = x;
			pacman.style.transform = 'rotate(-90deg)';
			break;
		case 's':
			y += 10;
			pacman.style.top = y + 'px';
			pacman.style.left = x;
			pacman.style.transform = 'rotate(90deg)';
			break;
		case 'a':
			x -= 10;
			pacman.style.top = y;
			pacman.style.left = x + 'px';
			pacman.style.transform = 'rotate(-180deg) rotateX(180deg)';
			break;
	}
});
