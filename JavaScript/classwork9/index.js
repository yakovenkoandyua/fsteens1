const textIpunt = document.getElementById('text');

// textIpunt.addEventListener('change', event => {
// 	console.log(event.target.value);
// });

// const button = document.getElementById('btn');

// button.addEventListener('click', event => {
// 	// console.log(event.target.id);
// 	// console.dir(event);
// });

// const allItems = document.querySelectorAll('.items');
// const allTitle = document.querySelectorAll('.title');

// console.log(allTitle.getAttribute('data-content'));
// console.log(allTitle.dataset.content);

// allItems.forEach(item => {
// 	item.addEventListener('click', event => {
// 		allItems.forEach(elem => {
// 			elem.classList.remove('active-item');
// 		});
// 		event.target.classList.add('active-item');

// 		allTitle.forEach(function (content) {
// 			if (content.dataset.content === event.target.dataset.title) {
// 				content.classList.add('active-content');
// 			} else {
// 				content.classList.remove('active-content');
// 			}
// 		});
// 	});
// });
const keys = document.querySelectorAll('.letter');

window.addEventListener('keyup', event => {
	keys.forEach(item => {
		if (event.key.toLocaleLowerCase() === item.dataset.key) {
			item.classList.add('active');
		} else {
			item.classList.remove('active');
		}
	});
});
